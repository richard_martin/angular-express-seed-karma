#!/bin/bash

BASE_DIR=`dirname $0`

echo $BASE_DIR

echo ""
echo "Starting Testacular Server (http://vojtajina.github.com/testacular)"
echo "-------------------------------------------------------------------"

karma start $BASE_DIR/config/karma.conf.js $*
